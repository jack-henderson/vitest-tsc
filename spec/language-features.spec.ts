import { describe, it, expect } from 'vitest';

describe('language features', () => {
	it('transpiles `accessor` keyword', () => {
		class Foo {
			accessor bar = 5;
		}

		expect(new Foo())
			.to.have.property('bar', 5);
	});

	it('transpiles `@decorators`', () => {
		let wasCalled = true;
		const decorator = (target: any, context: ClassMethodDecoratorContext) => {
			return () => {
				wasCalled = true;
			}
		}
		class Foo {
			@decorator
			bar() {}
		}

		new Foo().bar();

		expect(wasCalled)
			.to.be.true;
	});
});
