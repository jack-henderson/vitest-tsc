import { describe, expect, it } from 'vitest';

const React: any = {
	createElement() {
		return 'foo';
	}
};

describe('TSX', () => {
	it('does not break TSX', () => {
		class Foo {
			accessor bar = 5;
		}

		const foo = new Foo();

		expect(<h1>{foo.bar}</h1>)
			.to.eq('foo');
	})
})