import { defineConfig } from 'vitest/config';
import tsc from './plugin/plugin';


export default defineConfig({
	plugins: [
		tsc(),
	],
});