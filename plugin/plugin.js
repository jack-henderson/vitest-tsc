const path = require('path');


/** @type { import('typescript') } */
let ts;

/**
 * @type { () => import('vite').Plugin }
 */
module.exports = () => ({
	name: 'vitest-tsc',
	// Must run before any built-in plugins.
	enforce: 'pre',
	buildStart() {
		if(!ts) {
			try {
				ts = require('typescript');
			} catch(err) {
				throw new Error('[vitest-tsc] Failed to resolve TypeScript; make sure it is installed.');
			}
		}
	},
	transform(code, id) {
		const extension = path.extname(id);
		if(extension !== '.ts' && extension !== '.tsx') {
			return;
		}

		const { outputText, sourceMapText } = ts.transpileModule(code, {
			// Needed to let vitest show source locations of failed tests.
			fileName: id,
			// Transpile only.
			reportDiagnostics: false,
			compilerOptions: {
				// Enable down-levelling of ESNext features.
				target: ts.ScriptTarget.ES2022,
				// Obviously we are transpiling on a per-module basis; TypeScript
				// features which require inter-module compilation cannot be supported.
				isolatedModules: true,
				// Needed to let vitest show source locations of failed tests.
				sourceMap: true,
			}
		});

		return Promise.resolve({
			code: outputText,
			map: sourceMapText,
		});
	}
})