vitest-tsc
==========

[![npm](https://img.shields.io/npm/v/vitest-tsc)](https://www.npmjs.com/package/vitest-tsc)

---

**A tiny Vite plugin to transpile your tests with the TypeScript compiler.**

The goal of this plugin is to enable the use of [Vitest](https://vitest.dev)
with code using language features supported by the TypeScript compiler, but not
yet implemented in [esbuild](https://esbuild.github.io/).

For example: native `@decorators`, the `accessor` keyword, etc.

-----
Usage
-----

### Installation

```sh
npm i -D vitest-tsc
# or
yarn add -D vitest-tsc 
# or
pnpm i -D vitest-tsc
# etc...
```

### Configuration

Add the plugin to your `vitest.config.ts`:
```ts
import { defineConfig } from 'vitest/config';
import tsc from 'vitest-tsc';


export default defineConfig({
    plugins: [
	    tsc(),
    ]
});

```